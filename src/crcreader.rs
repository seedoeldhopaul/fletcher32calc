use std::fs::File;
use std::io::Read;
use std::io::Result;

pub struct CrcReader {
    pub crc: u32,
}

impl CrcReader {
    pub fn new(filename: &str) -> Result<Self> {
        let mut file = File::open(filename)?;
        let mut buffer = [0; 8 * 1024];
        let mut checksum = Fletcher32::new();

        loop {
            // Read in chunks of 8kB
            match file.read(&mut buffer)? {
                0 => break,
                len => checksum.update(&buffer[..len]),
            }
        }

        Ok(Self {
            crc: checksum.value(),
        })
    }
}

const MODULO: u32 = u16::MAX as u32;

struct Fletcher32 {
    c0: u32,
    c1: u32,
    csum: u32,
}

impl Fletcher32 {
    fn new() -> Self {
        Self {
            c0: 0,
            c1: 0,
            csum: 0,
        }
    }

    fn update(&mut self, data: &[u8]) {
        for x in data.chunks(2) {
            let lsb = x[0] as u32;
            let msb = x.get(1).copied().unwrap_or(0) as u32;
            let num = (msb << 8) | lsb;

            self.c0 = (self.c0 + num) % MODULO;
            self.c1 = (self.c1 + self.c0) % MODULO;
        }

        self.csum = (self.c1 << 16) | self.c0;
    }

    fn value(&self) -> u32 {
        self.csum
    }
}
