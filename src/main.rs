mod crcreader;
mod opendialog;

use cascade::cascade;
use gtk::prelude::*;
use gtk::{
    Align, Button, ButtonBuilder, Clipboard, Entry, EntryBuilder, GridBuilder, Inhibit,
    LabelBuilder, PositionType, Window, WindowBuilder, WindowType,
};
use relm::{connect, Relm, Update, Widget};
use relm_derive::Msg;

use crcreader::CrcReader;
use opendialog::OpenDialog;

#[derive(Msg)]
enum WinMsg {
    ChooseFile,
    CopyToClipboard,
    Quit,
}

#[derive(Clone)]
struct Widgets {
    file_path_entry: Entry,
    choose_file_button: Button,
    crc_display_entry: Entry,
    copy_button: Button,
    window: Window,
}

struct Win {
    widgets: Widgets,
}

impl Update for Win {
    type Model = ();
    type ModelParam = ();
    type Msg = WinMsg;

    fn model(_: &Relm<Self>, _: Self::ModelParam) -> Self::Model {
        ()
    }

    fn update(&mut self, event: Self::Msg) {
        match event {
            WinMsg::ChooseFile => {
                if let Some(filename) = OpenDialog::new(None).run() {
                    let filename = filename.to_str().unwrap();
                    self.widgets.file_path_entry.set_text(filename);
                    let reader = CrcReader::new(filename).unwrap();
                    let crc = format!("{:#x}", reader.crc);
                    self.widgets.crc_display_entry.set_text(&crc);
                }
            }
            WinMsg::CopyToClipboard => {
                let clipboard = Clipboard::get(&gdk::SELECTION_CLIPBOARD);
                clipboard.set_text(&self.widgets.crc_display_entry.get_text())
            }
            WinMsg::Quit => gtk::main_quit(),
        }
    }
}

impl Widget for Win {
    type Root = Window;

    fn root(&self) -> Self::Root {
        self.widgets.window.clone()
    }

    fn view(relm: &Relm<Self>, _: Self::Model) -> Self {
        let label_filename = LabelBuilder::new()
            .label("Filename: ")
            .halign(Align::Start)
            .margin_end(15)
            .build();

        let label_crc = LabelBuilder::new()
            .label("CRC: ")
            .halign(Align::Start)
            .margin_end(15)
            .build();

        let file_path_entry = EntryBuilder::new()
            .editable(false)
            .halign(Align::Fill)
            .placeholder_text("Path to file")
            .hexpand(true)
            .margin_end(10)
            .build();

        let crc_display_entry = EntryBuilder::new()
            .editable(false)
            .halign(Align::Fill)
            .placeholder_text("0xdeadbeef")
            .margin_end(10)
            .build();

        let choose_file_button = ButtonBuilder::new()
            .halign(Align::Baseline)
            .label("Choose File..")
            .tooltip_text("Select file to display CRC")
            .build();

        let copy_button = ButtonBuilder::new()
            .halign(Align::Baseline)
            .hexpand(true)
            .label("Copy to clipboard")
            .build();

        let grid = cascade! {
            GridBuilder::new().row_spacing(10).build();
            ..add(&label_filename);
            ..attach_next_to(&file_path_entry, Some(&label_filename), PositionType::Right, 5, 1);
            ..attach_next_to(&choose_file_button, Some(&file_path_entry), PositionType::Right, 1, 1);
            ..attach_next_to(&label_crc, Some(&label_filename), PositionType::Bottom, 1, 1);
            ..attach_next_to(&crc_display_entry, Some(&label_crc), PositionType::Right, 5, 1);
            ..attach_next_to(&copy_button, Some(&crc_display_entry), PositionType::Right, 1, 1);
        };

        let window = cascade! {
           WindowBuilder::new()
                .title("Fletcher32 Checksum Calculator")
                .type_(WindowType::Toplevel)
                .border_width(10)
                .default_width(600)
                .default_height(100)
                .resizable(false)
                .child(&grid)
                .build();
            ..show_all();
        };

        connect!(
            relm,
            window,
            connect_delete_event(_, _),
            return (Some(WinMsg::Quit), Inhibit(false))
        );

        connect!(
            relm,
            choose_file_button,
            connect_clicked(_),
            WinMsg::ChooseFile
        );

        connect!(
            relm,
            copy_button,
            connect_clicked(_),
            WinMsg::CopyToClipboard
        );

        Win {
            widgets: Widgets {
                file_path_entry,
                choose_file_button,
                crc_display_entry,
                copy_button,
                window,
            },
        }
    }
}

pub fn main() {
    Win::run(()).unwrap();
}
