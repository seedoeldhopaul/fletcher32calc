use cascade::cascade;
use gtk::prelude::*;
use gtk::*;
use std::path::PathBuf;

pub struct OpenDialog(FileChooserDialog);

impl OpenDialog {
    pub fn new(path: Option<PathBuf>) -> OpenDialog {
        let open_dialog = cascade! {
            FileChooserDialogBuilder::new()
                .title("Open")
                .action(FileChooserAction::Open)
                .build();
            ..add_button("Cancel", ResponseType::Cancel.into());
            ..add_button("Open", ResponseType::Ok.into());
        };

        path.map(|p| open_dialog.set_current_folder(p));

        Self(open_dialog)
    }

    pub fn run(&self) -> Option<PathBuf> {
        if self.0.run() == ResponseType::Ok.into() {
            self.0.get_filename()
        } else {
            None
        }
    }
}

impl Drop for OpenDialog {
    fn drop(&mut self) {
        self.0.close();
    }
}
